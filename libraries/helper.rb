# frozen_string_literal: true

require 'ipaddr'
require 'socket'

module KubeadmCookbook
  #
  # Kubeadm cookbook helper methods
  #
  module Helper
    include Chef::Mixin::ShellOut
    # formatted_ip_address_version
    include KubeadmCookbook::Formatters
    # first_valid_master_node
    include KubeadmCookbook::Finders

    def build_kubeadm_version
      expected_version = kubernetes_version_from_attributes

      detected = detect_kubeadm_version_from(expected_version)

      # Return the given version from the attributes when no version found
      return expected_version if detected.empty?

      # Returns the first detected kubeadm version
      detected
    end

    def current_kubernetes_version
      kubeadm_version = build_kubeadm_version

      return unless kubeadm_version

      # 1.15.2-00 -> 1_15
      kubeadm_version.split('.').take(2).join('_')
    end

    # This method return:
    #
    # * in 'off' multi_master mode:
    #   the master kuebapi port
    #
    # * in 'centralized' multi_master mode:
    #   - when called by master nodes: the local HAproxy port
    #   - when called by worker nodes: the remote HAproxy port from the masters
    #
    # * in 'decentralized' multi_master mode:
    #   - when called by master nodes: the kubeapi port of the master node which
    #     has initialized the cluster (/!\ SPOF to be fixed! /!\)
    #   - when called by worker nodes: the local HAproxy port
    #
    # * in 'balanced' multi_master mode:
    #   the external load balancer port
    def detect_master_port(options = {})
      Chef::Log.warn "kubeadm: detect_master_port: options: #{options.inspect}"
      target = if options.key?(:as) && options[:as] == :master
                 node
               else
                 master = first_valid_master_node

                 unless master
                   Chef::Log.warn 'kubeadm: Cannot retrieve the master port ' \
                                  'as no master node was found.'
                   return
                 end

                 master
               end

      Chef::Log.warn "kubeadm: detect_master_port: target: #{target && target.name}"

      unless target
        Chef::Log.warn 'kubeadm: Unable to determine the master port to be ' \
                       'used.'

        return
      end

      Chef::Log.warn "kubeadm: target['kubeadm']['multi_master']: #{target['kubeadm']['multi_master'].inspect}"
      Chef::Log.warn "kubeadm: target['kubeadm']['kubeapi_port']: #{target['kubeadm']['kubeapi_port'].inspect}"
      Chef::Log.warn "kubeadm: target['kubeadm']['haproxy']: #{target['kubeadm']['haproxy'].inspect}"
      Chef::Log.warn "kubeadm: target['kubeadm']['external_lb_port']: #{target['kubeadm']['external_lb_port'].inspect}"

      case target['kubeadm']['multi_master']
      when 'off'
        target['kubeadm']['kubeapi_port'] || 6443
      when 'centralized'
        target['kubeadm']['haproxy']['bind_port'] || 9443
      when 'decentralized'
        if options.key?(:as) && options[:as] == :master
          # Control planes are communicating directly to the master which has
          # initialized the cluster /!\
          #
          # TODO : Master nodes should all use a local HAproxy, configured with
          #        all other master nodes, preventing SPOF.
          #        Here we should be using
          #        `node['kubeadm']['haproxy']['bind_port'] || 9443`
          target['kubeadm']['kubeapi_port'] || 6443
        else
          # Workers are pointing to their local HAproxy instance
          node['kubeadm']['haproxy']['bind_port'] || 9443
        end
      when 'balanced'
        target['kubeadm']['external_lb_port'] || 6443
      end
    end

    #
    # Returns the IP address that should be used in order to init or join a
    # Kubernetes cluster.
    #
    # When no master_ip_address attribute is set, this method returns the first
    # IP address which matches the given family (IPv4 or IPV6) from the given
    # network interface.
    #
    # Otherwise, when master_ip_address is set, and the options[:as] is equal to
    # :master, then master_ip_address attribute is returned.
    #
    def detect_node_ip_address(options = {})
      as_master = options.key?(:as) && options[:as] == :master

      Chef::Log.warn "kubeadm: detect_node_ip_address: as_master: #{as_master.inspect}"
      Chef::Log.warn "kubeadm: detect_node_ip_address: node['kubeadm']['master_ip_address']: #{node['kubeadm']['master_ip_address'].inspect}"

      if node['kubeadm']['master_ip_address'] && as_master
        Chef::Log.warn "kubeadm: returning the master_ip_address!"
        return node['kubeadm']['master_ip_address']
      end

      res = detect_ip_address_from(node)
      Chef::Log.warn "kubeadm: detect_ip_address_from(node) : #{res.inspect}"
      res
    end

    def fetch_certificate_key_from_master
      first_valid_master_node&.normal['kubeadm']['certificate_key']
    end

    def fetch_discovery_token_ca_cert_hash_from_master
      first_valid_master_node&.normal['kubeadm']['discovery_token_ca_cert_hash']
    end

    # Returns the IP address of another Kubernetes master node.
    def fetch_existing_master_node_ip_address
      master = first_valid_master_node

      unless master
        Chef::Log.warn 'kubeadm: Cannot retrieve a master IP address as ' \
                       'no master node was found.'

        return
      end

      first_master_ip_address = detect_ip_address_from(master)
      Chef::Log.warn 'kubeadm: Returning first master IP address ' \
                     "#{first_master_ip_address.inspect}"

      first_master_ip_address
    end

    def fetch_kube_config_from_master
      master = first_valid_master_node

      unless master
        Chef::Log.warn 'kubeadm: Cannot retrieve the kubeconfig file as ' \
                       'no master node was found.'

        return
      end

      Chef::Log.warn 'kubeadm: Trying to get the kubeconfig file from master ' \
                     "node #{master.name} which has the following " \
                     'kubeadm attriubtes: ' \
                     "#{master.normal['kubeadm'].inspect}"

      master.normal['kubeadm']['kube_config']
    end

    def fetch_token_from_master
      master = first_valid_master_node

      unless master
        Chef::Log.warn 'kubeadm: Cannot retrieve the kubeadm join token as ' \
                       'no master node was found.'

        return
      end

      Chef::Log.warn "kubeadm: master.normal['kubeadm']: #{master.normal['kubeadm'].inspect}"

      master.normal['kubeadm']['token']
    end

    def kubernetes_master_not_reachable?(ip = nil, port = nil)
      kubernetes_master_reachable?(ip, port) == false
    end

    def kubernetes_master_reachable?(ip = nil, port = nil, local: false)
      Chef::Log.warn "kubeadm: kubernetes_master_reachable?: local: #{local.inspect}"
      ip ||= detect_node_ip_address(as: :master)
      port ||= if local
                 node['kubeadm']['kubeapi_port']
               else
                 detect_master_port(as: :master)
               end

      Chef::Log.warn "kubeadm: kubernetes_master_reachable?: #{ip}:#{port} ..."

      Chef::Log.info 'kubeadm: Checking if Kubernetes is reachable ...'
      Chef::Log.warn "kubeadm: Checking TCP connection to #{ip}:#{port} ..."

      TCPSocket.new(ip, port).close

      Chef::Log.warn "kubeadm: TCP connection to #{ip}:#{port} succeeded!"
      true
    rescue Errno::ECONNREFUSED,
           Errno::EHOSTUNREACH,
           Errno::ENETUNREACH,
           Errno::ETIMEDOUT
      Chef::Log.warn "kubeadm: TCP connection to #{ip}:#{port} failed!"
      false
    end

    #
    # Tries to retrieve the expected version from the Policy group attributes
    # otherwise takes the one from the node itself.
    #
    def kubernetes_version_from_attributes
      if node.policy_group
        expected_group_version = expected_policy_group_kubeadm_version

        return expected_group_version if expected_group_version
      end

      node['kubeadm']['version'].to_s
    end

    #
    # Check if at least one master has already upgraded to the desired version.
    #
    # Return true if none of the masters have upgraded to the desired version,
    # otherwise return false.
    #
    def no_upgraded_master_yet?
      Chef::Log.warn 'kubeadm: no_upgraded_master_yet? ...'
      current_version = installed_package_version('kubeadm').scan(/([\d\.]+)\-/)
                                                            .flatten
                                                            .first
      Chef::Log.warn "kubeadm: current_version: #{current_version.inspect}"

      versions = shell_out(
        "kubectl get nodes | grep master | awk '{print $5}'"
      ).stdout.split("\n")

      if versions.nil? || versions.empty?
        Chef::Log.fatal 'kubeadm: Something prevented from getting nodes ' \
                        'details.'

        exit 1
      end

      versions.detect { |version| version.include?(current_version) }.nil?
    end

    #
    # Returns the IP address of a master node.
    #
    def search_master_ip_address(master = nil)
      target = master || node
      Chef::Log.warn "kubeadm: search_master_ip_address: target['kubeadm']: #{target['kubeadm'].inspect}"
      case target['kubeadm']['multi_master']
      when 'off', 'centralized'
        if master.nil? && target['kubeadm']['master_ip_address']
          return target['kubeadm']['master_ip_address']
        end
      when 'balanced'
        return target['kubeadm']['external_lb_address']
      end

      # Otherwise,
      # - if 'off' or 'centralized', a node is given or
      # the 'master_ip_address' is not set
      # - if 'decentralized'
      #
      # return the given or first valid node IP address
      first_master = master || first_valid_master_node

      return unless first_master

      detect_ip_address_from(first_master)
    end

    #
    # Detects Kubernetes upgrade "request".
    #
    # 1. Compares the installed kubeadm version with the version from the
    # attributes
    # 2. Compares the installed kubelet version with the installed kubeadm
    # version so that in the case a Kubernetes upgrades didn't went until the
    # end (last upgrade step is to upgrade kubelet) the kubernetes upgrade will
    # be retried.
    # 3. When there is a difference in the above checks, then a Kubernetes
    # upgrade is required
    # 4. Otherwise there's no Kubernetes upgrade.
    #
    def upgrading_kubernetes?
      Chef::Log.warn 'kubeadm: upgrading_kubernetes? ...'
      current_version = installed_package_version('kubeadm')
      Chef::Log.warn "kubeadm: current_version: #{current_version.inspect}"

      if current_version.empty?
        Chef::Log.warn 'kubeadm: Kubernetes not yet installed, therefore no ' \
                       'need for an upgrade.'

        # Not yet installed, so couldn't be an upgrade
        return false
      end

      # kubeadm installed version is different than the requested one, so this
      # is the starting of Kubernetes upgrade.
      if kubeadm_version_from_attributes_is_different_than?(current_version)
        Chef::Log.warn 'kubeadm: Kubernetes upgrade detected, performing it ...'
        return true
      end

      # Installed kubeadm and kubelet versions are different, so an unfinished
      # upgrade is detected.
      if kubelet_version_is_different_than?(current_version)
        Chef::Log.warn 'kubeadm: Unfinished Kubernetes migration detected, ' \
                       'running the upgrade process again ...'
        return true
      end

      # Otherwise the kubeadm package is installed, the version is the same as
      # the one from the Policyifle attributes and kubeadm/kubelet installed
      # versions are the same, so there is no need for an upgrade.
      Chef::Log.warn 'kubeadm: No kubernetes upgrade, returning false ...'
      false
    end

    private

    # Gets all the available versions, makes a table of it, and returns only the
    # version column (Column with index 1)
    def available_kubeadm_versions
      @available_kubeadm_versions ||= begin
        versions = shell_out('apt-cache madison kubeadm').stdout.split("\n")
        versions.map { |version| version.split(' | ')[1].strip }
      end
    end

    def detect_ip_address_from(node)
      interface = node['kubeadm']['interface']
      Chef::Log.warn "kubeadm: interface: #{interface.inspect}"
      unless interface
        Chef::Log.warn 'kubeadm: This node has no value for the "interface" ' \
                       'attribute, which is not supported. Please update the ' \
                       'node attribute and provision it again.'

        raise
      end

      ip_address_version = node['kubeadm']['ip_address_version']

      network_interface = node['network']['interfaces'][interface]
      Chef::Log.warn "kubeadm: network_interface: #{network_interface.inspect}"
      unless network_interface
        Chef::Log.warn "kubeadm: The node #{node['hostname']} was supposed " \
                       "to have a network interface named #{interface} but " \
                       "it doesn't seem to have one with that name. " \
                       'Available network interfaces are ' \
                       "#{node['network']['interfaces'].keys.join(' ')}."

        ifconfig_shell = shell_out('ifconfig | grep k8sstg -A 1 | grep inet |' \
                                   "awk '{print $2}'")
        if ifconfig_shell.exitstatus.zero?
          ipaddress = ifconfig_shell.stdout.gsub(/\n/, '')
          Chef::Log.warn 'kubeadm: You have inconsitent Ohai data! The IP ' \
                         "address #{ipaddress} has been found using the " \
                         "ifconfig filtering on interface #{interface}!"
          Chef::Log.warn 'kubeadm: Using the IP address from ifconfig ...'
          return ipaddress
        else
          Chef::Log.warn "kubeadm: Checking #{interface} network interface " \
                         'using ifconfig failed (exit: ' \
                         "#{ifconfig_shell.exitstatus})"
        end

        raise
      end

      addresses = network_interface['addresses']

      unless addresses
        Chef::Log.warn "kubeadm: The node #{node['hostname']} has the " \
                       "expected network interface named #{interface} but " \
                       "that interface doesn't have an `addresses` attribute!"
        Chef::Log.warn 'kubeadm: Please double check the Ohai network ' \
                       "attriubtes for the network interface #{interface}. " \
                       'Convering that node could solve this issue as it ' \
                       'would refresh the Ohai attributes.'

        raise
      end

      detected = addresses.detect do |address|
        # `address` is an array of this form:
        #  - MAC address: ["08:00:27:93:8E:9C", {"family"=>"lladdr"}]
        #  - IPv4: ["172.28.128.200", {"family"=>"inet", ... }]
        #  - IPV6: ["fe80::a00:27ff:fecc:570a", {"family"=>"inet6", ... }]
        #
        # address.first returns the first array element, so "08:00:27:93:8E:9C",
        # "172.28.128.200" and "fe80::a00:27ff:fecc:570a" in this example.
        matching_network_address_version?(address.first, ip_address_version)
      end

      detected&.first
    end

    #
    # Try to match, with a regex, a version and return it, otherwise show a
    # warning message and the available versions.
    #
    def detect_kubeadm_version_from(expected_version)
      versions = available_kubeadm_versions

      return versions.first if expected_version == 'latest'

      detected = versions.detect { |version| version =~ /#{expected_version}/ }

      unless detected
        Chef::Log.warn 'kubeadm: WARNING: The kubeadm version you asked ' \
                       "\"#{expected_version}\" couldn't be found, the " \
                       'install command will fail. Here are the available ' \
                       "version:\n\n#{versions.join("\n")}"
      end

      detected
    end

    def expected_policy_group_kubeadm_version
      group = node.policy_group

      node[group] && node[group]['kubeadm'] &&
        node[group]['kubeadm']['version'].to_s
    end

    def installed_package_version(package)
      shell_out(
        "dpkg -s #{package} 2>/dev/null | grep Version | awk '{print $2}'"
      ).stdout.gsub(/\n/, '')
    end

    def kubeadm_version_from_attributes_is_different_than?(version)
      Chef::Log.warn "kubeadm: kubeadm_version_from_attributes_is_different_than?(#{version.inspect}) ..."
      kubeadm_version_from_attributes = build_kubeadm_version
      Chef::Log.warn "kubeadm: kubeadm_version_from_attributes: #{kubeadm_version_from_attributes.inspect}"

      res = kubeadm_version_from_attributes != version
      Chef::Log.warn "kubeadm: kubeadm_version_from_attributes_is_different_than? Returning #{res.inspect} ..."
      res
    end

    def kubelet_version_is_different_than?(version)
      Chef::Log.warn "kubeadm: kubelet_version_is_different_than?(#{version.inspect}) ..."
      kubelet_installed_version = installed_package_version('kubelet')
      Chef::Log.warn "kubeadm: kubelet_installed_version: #{kubelet_installed_version.inspect}"

      res = kubelet_installed_version != version
      Chef::Log.warn "kubeadm: kubelet_version_is_different_than? Returning #{res.inspect} ..."
      res
    end

    def matching_network_address_version?(address, ip_address_version = nil)
      case ip_address_version || formatted_ip_address_version
      when 'ipv4' then IPAddr.new(address).ipv4?
      when 'ipv6' then IPAddr.new(address).ipv6?
      end
    rescue IPAddr::InvalidAddressError
      false
    end
  end
end
