# frozen_string_literal: true

module KubeadmCookbook
  #
  # Formatter methods used across the kubeadm cookbook.
  #
  # All the methods are named `formatted_` and then the attribute name so that
  # you know it's not the raw attribute, and it's coming from this file.
  #
  module Formatters
    def formatted_cni_driver_name
      if Chef::Config.policy_group
        return unless node[Chef::Config.policy_group]['kubeadm']['cni_driver']

        node[Chef::Config.policy_group]['kubeadm']['cni_driver']['name'].downcase
      else
        return unless node['kubeadm']['cni_driver']

        node['kubeadm']['cni_driver']['name'].downcase
      end
    end

    def formatted_ip_address_version
      if Chef::Config.policy_group
        node[Chef::Config.policy_group]['kubeadm']['ip_address_version'].downcase
      else
        node['kubeadm']['ip_address_version'].downcase
      end
    end
  end
end
