# frozen_string_literal: true

require 'yaml'

module KubeadmCookbook
  #
  # Calico network driver helpers.
  # https://www.projectcalico.org/
  #
  module Calico
    def calico_url_with_version
      url = "https://docs.projectcalico.org/#{calico_version}/manifests" \
            '/calico.yaml'

      Chef::Log.warn "\nkubeadm: Downloading Calico YAML file at #{url} ..."
      url
    end

    private

    def calico_version
      version = version_from_attributes

      if version.nil? || version == 'master' || version == 'latest'
        'master'
      else
        "v#{version}"
      end
    end

    def version_from_attributes
      if Chef::Config.policy_group
        return nil unless node[Chef::Config.policy_group]['kubeadm']['cni_driver']

        node[Chef::Config.policy_group]['kubeadm']['cni_driver']['version']
      else
        return nil unless node['kubeadm']['cni_driver']

        node['kubeadm']['cni_driver']['version']
      end
    end
  end
end
