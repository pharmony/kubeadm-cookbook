# frozen_string_literal: true

module KubeadmCookbook
  #
  # Kubeadm cookbook methods to check and disable the swap partition using
  # systemd.
  #
  module Systemd
    include Chef::Mixin::ShellOut

    def node_has_systemd?
      Chef::Log.info 'kubeadm: Checking if systemd is present ...'
      systemctl_path = shell_out('which systemctl || echo ""').stdout

      present = systemctl_path.empty? == false

      Chef::Log.info "kubeadm: systemd is#{' not' unless present} present!"

      present
    end

    def systemd_has_swap_unit?
      Chef::Log.info 'kubeadm: Checking if systemd has a swap unit file ...'
      present = systemd_swap_unit_name.empty? == false

      Chef::Log.info "kubeadm: systemd does#{' not' unless present} have a " \
                     'swap unit file!'

      present
    end

    def systemd_swap_unit_is_running?
      Chef::Log.info 'kubeadm: Checking if systemd swap unit file is active ...'

      active = systemd_swap_unit_active == 'active'

      Chef::Log.info 'kubeadm: systemd swap unit file ' \
                     "is#{' not' unless active} active!"

      active
    end

    def systemd_swap_unit_name
      stdout = shell_out("#{systemd_swap_command} | awk '{print $1}'").stdout
      name = stdout.gsub(/\t/, '').gsub(/\n/, '')

      Chef::Log.info "kubeadm: systemd swap unit file name is #{name.inspect}"

      name
    end

    private

    def systemd_swap_command
      "systemctl list-units --all --no-legend *.swap | grep -v 'dev-disk-by'"
    end

    def systemd_swap_unit_active
      stdout = shell_out("#{systemd_swap_command} | awk '{print $3}'").stdout
      active = stdout.gsub(/\t/, '').gsub(/\n/, '')

      Chef::Log.info 'kubeadm: systemd swap unit file active is ' \
                     "#{active.inspect}"

      active
    end
  end
end
