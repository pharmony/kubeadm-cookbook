#!/usr/bin/env bash

#
# Runs the kubeadm milti-master install tests.
#

# Usage:
# bundle exec bin/test-kubernetes-multi-master [kubernetes version] [platform]
#
# Examples:
#
# Latest Kubernetes version with default platform and CNI driver:
# $ bundle exec bin/test-kubernetes-multi-master
#
# Kubernetes 1.15 with with default platform and CNI driver:
# $ bundle exec bin/test-kubernetes-multi-master 1.15
#
# Kubernetes 1.16 with Debian Stretch 64 bits and default CNI driver:
# $ bundle exec bin/test-kubernetes-multi-master 1.15 debian-stretch64
#
# Kubernetes 1.17 with Debian Buster 64 bits and the Flannel CNI driver
# $ bundle exec bin/test-kubernetes-multi-master 1.16 debian-buster64 flannel

# In order to keep a short .kitchen.yml file, only few Kubernetes versions are
# supported.
# This variable allows to tell what is the lower Kubernetes version the script
# allow to test.
MIN_SUPPORTED_VERSION='1.15'

K8S_VERSION=${1:-latest}
PLATFORM_VERSION=${2:-debian-buster64}
CNI_DRIVER=${3:-flannel}
KITCHEN_ADDITIONAL_FLAGS=()

trap 'echo CTRL + C detected, stopping ...; exit' INT

# Parse Kubernetes version
K8S_VERSION_MAJOR_VERSION=`echo $K8S_VERSION | awk -F '.' '{print $1}'`
K8S_VERSION_MINOR_VERSION=`echo $K8S_VERSION | awk -F '.' '{print $2}'`

if [ $K8S_VERSION = "latest" ]; then
  K8S_FINAL_VERSION="latest"
else
  K8S_FINAL_VERSION="$K8S_VERSION_MAJOR_VERSION-$K8S_VERSION_MINOR_VERSION"
fi

# Checking for minimal allowed Kubernetes version
MIN_SUPPORTED_MAJOR=`echo $MIN_SUPPORTED_VERSION | awk -F '.' '{print $1}'`
MIN_SUPPORTED_MINOR=`echo $MIN_SUPPORTED_VERSION | awk -F '.' '{print $2}'`

if [ $K8S_VERSION != "latest" ]; then
  if [[ $K8S_VERSION_MAJOR_VERSION -lt $MIN_SUPPORTED_MAJOR || $K8S_VERSION_MINOR_VERSION -lt $MIN_SUPPORTED_MINOR ]]; then
    echo "ERROR: The minimal supported Kubernetes version is $MIN_SUPPORTED_VERSION."
    echo "       Please choose a higher version."

    exit 1
  fi
fi

echo "Testing the Multi-Master mode of the kubeadm cookbook with Kubernetes:"
echo ""
echo "    Kubernetes version: $K8S_VERSION"
echo "    Platform: $PLATFORM_VERSION"
echo "    CNI driver: $CNI_DRIVER"

CNI_DRIVER_NAME=".$CNI_DRIVER"
if [ "$CNI_DRIVER_NAME" = ".flannel" ]; then
  CNI_DRIVER_NAME=""
fi

if [ ! -f ".kitchen$CNI_DRIVER_NAME.yml" ]; then
  echo "The Kitchen file '.kitchen$CNI_DRIVER_NAME.yml' doesn't exist."
  exit 1
fi

if [[ $DEBUG == "true" ]]
then
  echo "    Debug enabled!"
  KITCHEN_ADDITIONAL_FLAGS+=("--log-level debug")
  KITCHEN_ADDITIONAL_FLAGS+=("--debug")
fi

echo ""
echo "=> Destroying kitchens if existing ..."
# Destroy master 1 and 2, and worker kitchens
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen destroy "^k8s-$K8S_FINAL_VERSION-mutli-master-[\w]+-$CNI_DRIVER-$PLATFORM_VERSION"

# ~~~~ Master 1 ~~~~
MASTER_KITCHEN_NAME="k8s-$K8S_FINAL_VERSION-mutli-master-1-$CNI_DRIVER-$PLATFORM_VERSION"

echo ""
echo "=> Recreating Kubernetes first master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen create $MASTER_KITCHEN_NAME || exit 1
#
# ---- ONLY FOR UBUNTU ----
#
# Rename the master network interface
#
if [[ "$PLATFORM_VERSION" =~ 'ubuntu' ]]; then
  NETMAC=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME \
            -c "sudo ifconfig | \
                grep 'inet 172.28.128.' -A 5 | \
                grep ether | \
                awk '{print \$2}'" | \
           tail -n 1 | \
           awk '{print $1}')

  echo "Retrieved network interface MAC address: $NETMAC"
  echo "Renaming network interface as eth1 ..."

  # Updates the netplan file
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME \
    -c "echo '      match:
        macaddress: $NETMAC
      set-name: eth1' | \
        sudo tee -a /etc/netplan/50-vagrant.yaml"

  echo "Applying update ..."
  # Generate and apply the changes
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME -c "sudo netplan generate"
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME -c "sudo netplan apply"

  # Restart the VM
  cd .kitchen/kitchen-vagrant/$MASTER_KITCHEN_NAME
  vagrant reload
  cd -
fi
#

echo ""
echo "=> Converging Kubernetes first master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen converge ${KITCHEN_ADDITIONAL_FLAGS[@]} $MASTER_KITCHEN_NAME || exit 1
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen setup $MASTER_KITCHEN_NAME

echo ""
echo "=> Verifying Kubernetes first master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen verify $MASTER_KITCHEN_NAME || exit 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Updates the node fixture file
#
# This is mandatory so that the worker node knows the required discovery ca cert
# hash built by the master node.
#
echo ""
echo "=> Updating Kubernetes first master node JSON fixture file ..."
MASTER_1_FIXTURE_FILE_PATH="./test/fixtures/multi-master/phase-2/$CNI_DRIVER/master-1-$CNI_DRIVER.vagrant.test.json"

if [ ! -f "$MASTER_1_FIXTURE_FILE_PATH" ]; then
  echo "The fixture file $MASTER_1_FIXTURE_FILE_PATH doesn't exist."
  exit 1
fi

# Fetch a valid token to be used by nodes to join the master.
KUBEADM_TOKEN=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME \
                  -c "kubeadm token list | \
                      grep -v TOKEN | \
                      grep -v '<invalid>' | \
                      grep authentication | \
                      head -n 1 | \
                      awk '{print \$1}'" | \
                tail -n 1 | \
                tr -d '[:space:]')
echo "Saving token $KUBEADM_TOKEN to $MASTER_1_FIXTURE_FILE_PATH ..."
sed -i -e 's/"token": ".*"/"token": "'$KUBEADM_TOKEN'"/' \
  $MASTER_1_FIXTURE_FILE_PATH

# Fetch the dicovery token ca cert hash to be used by nodes to join the master.
DISCOVERY_TOKEN=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME \
                    -c "openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | \
                        openssl rsa -pubin -outform der 2>/dev/null | \
                        openssl dgst -sha256 -hex | \
                        sed 's/^.* //'" | \
                  tail -n +2 | \
                  tr -d '[:space:]')
echo "Saving cert Hash $DISCOVERY_TOKEN to $MASTER_1_FIXTURE_FILE_PATH ..."
sed -i -e 's/"discovery_token_ca_cert_hash": ".*"/"discovery_token_ca_cert_hash": "'$DISCOVERY_TOKEN'"/' \
  $MASTER_1_FIXTURE_FILE_PATH

# Fetch the keepalived password and update the master node fixture file.
KEEPALIVED_PASSWORD=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $MASTER_KITCHEN_NAME \
                        -c "sudo cat /etc/keepalived/conf.d/keepalived_vrrp_instance__kubernetes-vip__.conf | \
                            grep auth_pass" | \
                      awk '{print $2}' | \
                      tail -n 1 | \
                      tr -d '[:space:]')
echo "Saving keepalived password $KEEPALIVED_PASSWORD to $MASTER_1_FIXTURE_FILE_PATH ..."
sed -i -e 's/"authentication_password": ".*"/"authentication_password": "'$KEEPALIVED_PASSWORD'"/' \
  $MASTER_1_FIXTURE_FILE_PATH

# Retrieves the Kube config file as a base64 string
KUBE_CONFIG=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" \
              kitchen exec $MASTER_KITCHEN_NAME \
                -c "sudo cat /etc/kubernetes/admin.conf" | \
              grep apiVersion -A 99 | \
              base64 | \
              tr -d \\n)
echo "Saving kube config to $MASTER_1_FIXTURE_FILE_PATH ..."
sed -i -e 's/"kube_config": ".*"/"kube_config": "'$KUBE_CONFIG'"/' \
  $MASTER_1_FIXTURE_FILE_PATH
if [ ! "$?" -eq 0 ]; then
  echo "ERROR: Something prevented to update the '$MASTER_1_FIXTURE_FILE_PATH' file with the kube config."
  echo $KUBE_CONFIG

  exit 1
fi
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~ Worker ~~~~
WORKER_KITCHEN_NAME="k8s-$K8S_FINAL_VERSION-mutli-master-worker-$CNI_DRIVER-$PLATFORM_VERSION"

echo ""
echo "=> Recreating Kubernetes worker node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen destroy $WORKER_KITCHEN_NAME
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen create $WORKER_KITCHEN_NAME || exit 1
#
# ---- ONLY FOR UBUNTU ----
#
# Rename the worker network interface
#
if [[ "$PLATFORM_VERSION" =~ 'ubuntu' ]]; then
  NETMAC=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $WORKER_KITCHEN_NAME \
            -c "sudo ifconfig | \
                grep 'inet 172.28.128.' -A 5 | \
                grep ether | \
                awk '{print \$2}'" | \
           tail -n 1 | \
           awk '{print $1}')

  echo "Retrieved network interface MAC address: $NETMAC"
  echo "Renaming network interface as eth1 ..."
  # Updates the netplan file
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $WORKER_KITCHEN_NAME \
    -c "echo '      match:
        macaddress: $NETMAC
      set-name: eth1' | \
        sudo tee -a /etc/netplan/50-vagrant.yaml"
  # Generate and apply the changes
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $WORKER_KITCHEN_NAME -c "sudo netplan generate"
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $WORKER_KITCHEN_NAME -c "sudo netplan apply"

  # Restart the VM
  cd .kitchen/kitchen-vagrant/$WORKER_KITCHEN_NAME
  vagrant reload
  cd -
fi
#
#
echo "==> Converging $WORKER_KITCHEN_NAME ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen converge ${KITCHEN_ADDITIONAL_FLAGS[@]} $WORKER_KITCHEN_NAME || exit 1
#
# Wait for the worker node to be Ready
declare -i retry_count=0
PREVIOUS_WORKER_CNI_POD_STATUS=""

echo ""
echo "=> Waiting the worker node to be Ready ..."
while [ $retry_count -le 200 ]
do
  WORKER_CNI_POD_STATUS=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" \
                          kitchen exec $MASTER_KITCHEN_NAME \
                            -c "kubectl get nodes | \
                                grep worker-$CNI_DRIVER" | \
                          tail -n 1 | \
                          awk '{print $2}')

  if [ "$WORKER_CNI_POD_STATUS" != "Ready" ]; then
    if [ $retry_count == 0 ]; then
      # Message shown on the very first attempt
      echo "Worker node is $WORKER_CNI_POD_STATUS, waiting for worker node to be Ready ..."
    else
      # Detects status changes in order to avoids repeating the same message
      # again and again.
      if [ "$WORKER_CNI_POD_STATUS" != "$PREVIOUS_WORKER_CNI_POD_STATUS" ]; then
        echo "Worker node status changed to $WORKER_CNI_POD_STATUS (After $retry_count seconds)"
        PREVIOUS_WORKER_CNI_POD_STATUS="$WORKER_CNI_POD_STATUS"
      fi
    fi

    ((retry_count++))
    sleep 1
  else
    echo "--> Worker node is in the 'Ready' state!"

    # Leaves the loop as soon as the worker node status changes to "Ready"
    break
  fi
done
#
echo ""
echo "=> Setuping Kubernetes worker node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen setup $WORKER_KITCHEN_NAME

echo ""
echo "=> Verifying Kubernetes worker node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen verify $WORKER_KITCHEN_NAME || exit 1

# ~~~~ Master 2 ~~~~
SECOND_MASTER_KITCHEN_NAME="k8s-$K8S_FINAL_VERSION-mutli-master-2-$CNI_DRIVER-$PLATFORM_VERSION"

echo ""
echo "=> Recreating Kubernetes second master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen destroy $SECOND_MASTER_KITCHEN_NAME
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen create $SECOND_MASTER_KITCHEN_NAME || exit 1
#
# ---- ONLY FOR UBUNTU ----
#
# Rename the second master network interface
#
if [[ "$PLATFORM_VERSION" =~ 'ubuntu' ]]; then
  NETMAC=$(KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $SECOND_MASTER_KITCHEN_NAME \
            -c "sudo ifconfig | \
                grep 'inet 172.28.128.' -A 5 | \
                grep ether | \
                awk '{print \$2}'" | \
           tail -n 1 | \
           awk '{print $1}')

  echo "Retrieved network interface MAC address: $NETMAC"
  echo "Renaming network interface as eth1 ..."
  # Updates the netplan file
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $SECOND_MASTER_KITCHEN_NAME \
    -c "echo '      match:
        macaddress: $NETMAC
      set-name: eth1' | \
        sudo tee -a /etc/netplan/50-vagrant.yaml"
  # Generate and apply the changes
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $SECOND_MASTER_KITCHEN_NAME -c "sudo netplan generate"
  KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen exec $SECOND_MASTER_KITCHEN_NAME -c "sudo netplan apply"

  # Restart the VM
  cd .kitchen/kitchen-vagrant/$SECOND_MASTER_KITCHEN_NAME
  vagrant reload
  cd -
fi
#

echo ""
echo "=> Converging Kubernetes second master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen converge ${KITCHEN_ADDITIONAL_FLAGS[@]} $SECOND_MASTER_KITCHEN_NAME || exit 1
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen setup $SECOND_MASTER_KITCHEN_NAME

echo ""
echo "=> Verifying Kubernetes second master node ..."
KITCHEN_YAML=".kitchen$CNI_DRIVER_NAME.yml" kitchen verify $SECOND_MASTER_KITCHEN_NAME || exit 1
