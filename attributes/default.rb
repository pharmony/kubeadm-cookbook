# frozen_string_literal: true

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#                                Kubeadm Details
#                                ---------------
#
# Choose the kubeadm version and your Kubernetes cluster token.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# [Mandatory] Kubeadm version to be installed on your node.
#
# When is `latest`, no versions are given to the node package manager, so that
# it will install the newest one. (Not recommended, only for testing newer
# Kubernetes version)
default['kubeadm']['version'] = nil

# [Optional] The kubeadm --certificate-key
#
# When `nil`, it will be automatically generated using
# the `kubeadm alpha certs certificate-key` command, otherwise if you set a key
# it will be used instead.
default['kubeadm']['certificate_key'] = nil

# [Optional] Kubernetes Cluster Name
default['kubeadm']['cluster_name'] = 'kubernetes'

# [Optional] Extra Certificat SANs (Subject Alternative Names)
#
# In the case you need to add more names to the Kubernetes certificat, like the
# load balancer FQDN, here is the right plus to do so.
default['kubeadm']['extra_cert_sans'] = []

# [Optional] Enables the Pod security policies
#
# Setting this attribut to true will enable the PSP feature on the master nodes.
default['kubeadm']['psp'] = false

# [Optional] The Kubelet cgroup driver
#
# Should be the same as the Docker cgroup driver. If not specified, 'cgroupfs'
# is the default value.
default['kubeadm']['kubelet']['cgroup_driver'] = 'cgroupfs'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#                            Node Network Information
#                            ------------------------
#
# Those information are used to configure your Kubernetes cluster node.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# [Mandatory] Node network interface to be used by Kubernetes.
#
# The node IP address (ipv4 or ipv6) used to run kubeadm command will be taken
# from that network interface.
default['kubeadm']['interface'] = nil

# [Mandatory] Network interface address version (ipv4 or ipv6)
#
# Given a network interface support ipv4 and ipv6, the node IP address to be
# used with kubeadm will be choosen from:
#  - the node network interface (See default['kubeadm']['interface'])
#  - the IP version
#
# So that if the network interface has the following IP addresses:
#   - "08:00:27:3F:7D:71"
#   - "172.28.128.200"
#   - "fe80::a00:27ff:fe3f:7d71"
#
# And you specify version `ipv4`, "172.28.128.200" will be taken, but if you
# specify version `ipv6`, "fe80::a00:27ff:fe3f:7d71" will be taken.
default['kubeadm']['ip_address_version'] = 'ipv4'

# [Mandatory] Kubernetes API server port
#
# Kubernetes API server listening port.
default['kubeadm']['kubeapi_port'] = 6443

# [Optional] Configures the multi-master mode
#
# off
# ---
#
# Multi master mode is disabled and is the default value.
#
# centralized
# -----------
#
# Enables the multi master mode with master nodes be behind a load balancer, and
# worker nodes are joining through the load balancer's IP address.
#
# In this mode keepalived and HAproxy are installed on the master nodes.
# On the first bootstrapped node, it will initialize the cluster, while other
# masters will join the cluster through the keepalived VIP.
#
# keepalived allows to share a virtual IP address between a group of nodes using
# an election machanism.
# HAproxy load balances the requests between master nodes.
#
# This mode is made for the case where you're running on a private network or
# through a VPN, so that traifc encryption is made at network level.
#
# decentralized
# -------------
#
# Enables the multi master mode and install an HAproxy instance on the worker
# nodes, configured with the public IP addresses of the master nodes.
#
# Worker nodes will join the cluster through the HAproxy by pointing to its
# localhost.
# HAproxy will load balance worker's requests to each master nodes.
#
# This mode is made for the case where the CNI is doing the trafic encryption.
#
# balanced
#
# Here each and every nodes will use the bellow `external_lb_address` and
# `external_lb_port` to initialize/join the cluster and for worker nodes to join
# the control planes.
#
default['kubeadm']['multi_master'] = 'off'

# * When the multi_master is 'off'
#   ------------------------------
# This attribute is optional and allows you to force the IP address of
# the master node. This could be useful when the network interface used
# by the Kubernetes master has multiple IP addresses.
#
# * When the multi_master is 'centralized'
#   --------------------------------------
#
# This attribute is mandatory and it defines the virtual IP address of the
# load balancer (that will be installed and configured) used by Kubernetes
# members in order to communicate together.
# Keepalived will be used in order to provide the given IP address on
# the master nodes so that when one goes down, the other nodes detect it and
# run an election and update the node holding the VIP.
# See the "Keepalived" section at the bottom for custom settings.
#
# * When the multi_master is 'decentralized'
#   ----------------------------------------
#
# In this mode in one hand, the first control plane will use the node's
# IP address in order to initialize the cluster, then other control planes will
# search for the first control plan IP address from your Chef repository
# in order to join the cluster.
#
# In the other hand, the workers will have a local HAproxy configured with the
# master nodes, and will use it to access them.
#
# In other words, this parameter is completely ignored.
#
# * When the multi_master is 'balanced'
#   ----------------------------------------
#
# The `master_ip_address` attribute is completely ignored and the bellow
# `external_lb_address` and `external_lb_port` attributes are used instead.
#
default['kubeadm']['master_ip_address'] = nil

# Load balancer IP address or FQDN
default['kubeadm']['external_lb_address'] = nil
# Load balancer port will be 6443 if `nil`
default['kubeadm']['external_lb_port'] = 6443

# [Optional] Virtual network for pods on the node.
#
# This is the value passed to the `kubeadm init --pod-network-cidr` flag.
#
# For the flannel network driver, this attribute is ignore as the value is
# forced in order to match the flannel installation file.
# See https://stackoverflow.com/questions/48984659/understanding-kubeadm-init-command-for-flannel
# See `forced_or_initial_for_network_driver` from libraries/kubeadm_flags.rb
default['kubeadm']['pod_cidr'] = nil

# [Optional] Virtual IPs for services subnet
default['kubeadm']['service_cidr'] = '10.96.0.0/12'
# [Optional] Authoritative Kuberneres DNS server domain name
#
# This sets the Pods DNS FQDN.
# See https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/
default['kubeadm']['dns_domain'] = 'cluster.local'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#              Kubernetes Container Network Interface (CNI) Driver
#              ---------------------------------------------------
#                       /!\ Only for the master nodes /!\
#
# Configure the Pods networking within your Kubernetes cluster.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# [Mandatory] The Kubernetes CNI driver to be installed.
#
# Supported drive list:
#  - flannel (https://github.com/coreos/flannel)
#  - calico (https://www.projectcalico.org/)
default['kubeadm']['cni_driver']['name'] = 'flannel'

# Common CNI options
# ==================
#
# [Optional] CNI driver version to be installed.
#
# If is `latest`, `master` or `nil`, the version from the `master` branch
# will be used, otherwise the given tag name will be used.
# (So for flannel version v0.11.0, given the value `0.11.0` without the "v" as
# the recipe adds it itself).
default['kubeadm']['cni_driver']['version'] = 'latest'

# Calico CNI options
# ==================
#
# [Optional] Wireguard (https://www.wireguard.com/)
#
# This option install wireguard and configure Calico in order to use to encrypt
# network trafic.
# Default is disabled
default['kubeadm']['cni_driver']['calico']['wireguard']['enable'] = false

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#                        Keepalived Custom Attributes
#                      ---------------------------------
#                      /!\ Only in mutli-master mode /!\
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#
# Update the values here only in the case you know what you are doing.
# In the majority of the cases, the default values should work.
#
default['kubeadm']['keepalived']['virtual_router_id'] = 1
# Autogenerated when nil, then saved in the node's normal attributes.
# When autogenerated, other nodes will take the auto-generated password from
# the node that has a generated password in its attributes.
default['kubeadm']['keepalived']['authentication_password'] = nil
default['kubeadm']['keepalived']['unicast_src_ip'] = nil
default['kubeadm']['keepalived']['unicast_peer'] = nil
# [Optional] Slack WebHook URL
#
# When giving an URL, a Python script will be installed in the `/usr/local/bin`
# folder and will that will be used by Keepalived in order to notify about its
# state change.
default['kubeadm']['keepalived']['slack_webhook_url'] = nil
# [Optional] Systemd unit file linking
#
# This option allows you to link the keepalived.service unit with another one
# so that keepalived is restarted when this other service is restarted.
# That's helpful with a VPN service for example so that when the VPN service
# restarts, keepalived would be restarted too.
default['kubeadm']['keepalived']['systemd_partof'] = nil

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#                         HAproxy Custom Attributes
#                      ---------------------------------
#                      /!\ Only in mutli-master mode /!\
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

default['kubeadm']['haproxy']['bind_port'] = 9443
