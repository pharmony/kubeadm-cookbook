# frozen_string_literal: true

name              'kubeadm'
maintainer        'Pharmony SA'
maintainer_email  'dev@pharmony.lu'
license           'MIT'
description       'Installs Kubernetes using kubeadm'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '0.1.0'
chef_version      '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/pharmony/kubeadm-cookbook/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/pharmony/kubeadm-cookbook'

depends 'apt'

# ~~~~ Multi Master ~~~~
depends 'keepalived', '~> 5.0.0'

%w[debian].map do |platform|
  supports platform
end
