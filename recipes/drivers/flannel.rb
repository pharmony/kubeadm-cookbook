# frozen_string_literal: true

# `flannel_url_with_version`
Chef::Resource::RemoteFile.send(:include, KubeadmCookbook::Flannel)
# `kubernetes_master_reachable?`
Chef::Recipe.send(:include, KubeadmCookbook::Helper)
# `wait_command_output`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::StreamCommand)
# `update_flannel_config_file_at`
Chef::Resource::RubyBlock.send(:include, KubeadmCookbook::Flannel)

unless node['kubeadm']['multi_master'] == 'off'
  kube_up = kubernetes_master_reachable?(local: true)
  log 'is Kubernetes up and running?' do
    level :warn
    message "Kubernetes is#{' not' unless kube_up} running."
  end
end

#
# Installs the CoreOs Flannel network driver to an existing Kubernetes cluster.
#
# Download Flannel to /tmp/kube-flannel.yml
remote_file '/tmp/kube-flannel.yml' do
  action :create
  source(lazy { flannel_url_with_version })
  owner 'root'
  group 'root'
  mode '0644'

  only_if do
    if node['kubeadm']['multi_master'] == 'off'
      true
    else
      Chef::Log.warn 'kubeadm: [remote_file] In multi-master mode.'

      Chef::Log.warn "kubeadm: [remote_file] kube_up: #{kube_up.inspect}"
      if kube_up
        # Ensures the master node is in the Ready state
        Chef::Log.warn 'kubeadm: Checking master node status in Kubernetes ...'
        ready_shell = shell_out('kubectl get node $(hostname) | tail -n 1 | ' \
                                "awk '{print $2}' | grep Ready")
        if ready_shell.exitstatus.zero?
          Chef::Log.warn 'kubeadm: Master node is in Ready state'

          # Checks if the Flannel pods are present and in the Running state
          Chef::Log.warn 'kubeadm: Checking flannel pods status ...'
          flannel_status_shell = shell_out('kubectl get pods -n kube-system ' \
                                           '| grep kube-flannel | grep Running')
          if flannel_status_shell.exitstatus.zero?
            Chef::Log.warn 'kubeadm: Flannel pod is present and in the ' \
                           'Running state, abording flannel installation ...'
            false
          else
            Chef::Log.warn 'kubeadm: Flannel pods are not present, or not in ' \
                           'the Running state!'
            Chef::Log.warn "kubeadm: #{flannel_status_shell.stdout.inspect}"

            Chef::Log.warn 'Running the Flannel installation!'
            true
          end
        else
          Chef::Log.warn 'kubeadm: Master node is not in Ready state, ' \
                         'abording flannel installation ...'

          false
        end
      else
        false
      end
    end
  end
end

ruby_block 'update flannel iface' do
  action :run
  block { update_flannel_config_file_at('/tmp/kube-flannel.yml') }
  only_if 'test -f /tmp/kube-flannel.yml'
end

# flannel pod network
execute 'kubectl apply -f /tmp/kube-flannel.yml' do
  command 'kubectl apply -f /tmp/kube-flannel.yml'
  action :run
  only_if 'test -f /tmp/kube-flannel.yml'
end

# Wait for the Flannel kube-flannel pod. Name is something like
# `kube-flannel-ds-amd64-tbvzx`.
ruby_block 'wait for flannel' do
  action :run
  block do
    Chef::Log.warn 'kubeadm: Waiting for the flannel containers to boot ' \
                   '(timeout after 120 seconds) ...'

    command = <<~CMD
      kubectl get pods --namespace=kube-system | \
      grep kube-flannel | \
      awk '{print $3}'
    CMD

    begin
      # Runs the given command, and look at its ouput if it matches the expected
      # regex until the end of the timeout (120 seconds).
      # Each time the output changes, the block is executed with its output.
      wait_command_output(command, /Running/, timeout: 120) do |output|
        Chef::Log.warn "kubeadm: Flannel container status changed to #{output}."
      end
    rescue KubeadmCookbook::WaitCommandTimeoutError
      # When the timeout is reached, the WaitCommandTimeoutError is thrown
      Chef::Log.warn 'kubeadm: Waiting for the Flannel Kubernetes container ' \
                     'to be "Running" failed after 120 seconds.'
      Chef::Log.warn "kubeadm: It doesn't mean that the deployment failed, " \
                     'but you should check manually on the node the flannel ' \
                     'container status.'
      Chef::Log.warn 'kubeadm: You can have a look at the existing ' \
                     'containers with the following command:'
      Chef::Log.warn 'kubeadm: kubectl get pods --namespace=kube-system'
    end
  end
  only_if 'test -f /tmp/kube-flannel.yml'
end

file '/tmp/kube-flannel.yml' do
  action :delete
  only_if 'test -f /tmp/kube-flannel.yml'
end
