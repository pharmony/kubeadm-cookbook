# frozen_string_literal: true

# Inspec test for recipe .::master

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe command('swapon -s | wc -l | grep 0') do
  its('exit_status') { should eq 0 }
end

describe service('firewalld') do
  it { should_not be_running }
end

describe package('apt-transport-https') do
  it { should be_installed }
end

# describe apt('https://apt.kubernetes.io/') do
#   it { should exist }
#   it { should be_enabled }
# end

describe package('kubeadm') do
  it { should be_installed }
  it { should be_held }
end

describe package('kubelet') do
  it { should be_installed }
  it { should be_held }
end

describe package('kubectl') do
  it { should be_installed }
  it { should be_held }
end

describe package('psmisc') do
  it { should be_installed }
end

describe package('keepalived') do
  it { should be_installed }
end

describe file('/etc/keepalived/conf.d/global_defs.conf') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0640' }
  its('content') do
    should match(
      /global_defs {\n\s+router_id master-1-flannel\n\s+enable_script_security\n/
    )
  end
end

describe file('/etc/keepalived/conf.d/00_keepalived_vrrp_script__haproxy-check__.conf') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0640' }
  its('content') do
    should match(/vrrp_script haproxy-check {/)
    should match(%r{script "/usr/bin/killall -0 haproxy"})
    should match(/interval 2/)
    should match(/weight 50/)
  end
end

describe file('/etc/keepalived/conf.d/keepalived_vrrp_instance__kubernetes-vip__.conf') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0640' }
  its('content') { should match(/vrrp_instance kubernetes-vip/) }
  its('content') { should match(/state MASTER/) }
  its('content') { should match(/virtual_router_id 1/) }
  its('content') { should match(/interface eth1/) }
  its('content') { should match(/priority 110/) }
  its('content') do
    should match(/authentication {\n\s+auth_type PASS\n\s+auth_pass \w+/)
  end
  its('content') { should match(/virtual_ipaddress {\n\s+[\d\.]+/) }
  its('content') { should match(/track_script {\n\s+haproxy-check/) }
end

describe file('/usr/local/bin/keepalived_notify') do
  it { should exist }
  its('owner') { should eq 'keepalived_script' }
  its('group') { should eq 'keepalived_script' }
  its('mode') { should cmp '0755' }
end

describe package('haproxy') do
  it { should be_installed }
end

describe file('/etc/haproxy/haproxy.cfg') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
  its('content') do
    should match(%r{
      frontend\sapi-front
        \s+bind\s172.28.128.10:9443
        \s+bind\s172.28.128.200:9443
        \s+mode\stcp
        \s+option\stcplog
        \s+use_backend\sapi-backend
    }x)
  end
  its('content') do
    should match(%r{
      backend\sapi-backend
        \s+mode\stcp
        \s+option\stcp-check
        \s+balance\sroundrobin
        \s+default-server\sinter\s10s\s
                           downinter\s5s\s
                           rise\s2\s
                           fall\s2\s
                           slowstart\s60s\s
                           maxconn\s250\s
                           maxqueue\s256\s
                           weight\s100
        \s+server\smaster-1-flannel\s172.28.128.200:6443\scheck
    }x)
  end
end

describe service('kubelet') do
  it { should be_running }
end

describe file('/etc/kubernetes/kubelet.conf') do
  it { should exist }
end

describe file('/etc/default/kubelet') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
  its('content') do
    should match(%r{KUBELET_EXTRA_ARGS=.*--node-ip=172.28.128.200})
  end
end

describe file('.kube/config') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0644' }
end

describe file('/etc/kubernetes/manifests/kube-apiserver.yaml') do
  it { should exist }
  its('content') do
    should match(%r{--enable-admission-plugins=NodeRestriction})
  end
end

describe processes('dockerd') do
  it { should exist }
end

describe processes('kubelet') do
  it { should exist }
end

describe command("kubectl get nodes -o jsonpath='{.items[*].spec.podCIDR}'") do
  its('stdout') { should match(%r{10.244.0.0}) }
end

describe command('kubectl get pods --namespace=kube-system | ' \
                 'grep kube-flannel | ' \
                 'head -n 1 | ' \
                 "awk '{print $3}'") do
  its('stdout') { should eq "Running\n" }
end

describe command('kubectl get nodes | ' \
                 'grep master-1-flannel | ' \
                 "awk '{print $2}'") do
  its('stdout') { should eq "Ready\n" }
end
